package com.example.primero;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

public class MainActivity extends AppCompatActivity {
    TextInputLayout tilRut, tilFirstName, tilLastName, tilGender, tilAge, tilBirthday;
    Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tilRut = findViewById(R.id.activity_main_til_rut);
        tilFirstName = findViewById(R.id.activity_main_til_first_name);
        tilLastName = findViewById(R.id.activity_main_til_last_name);
        tilGender = findViewById(R.id.activity_main_til_gender);
        tilAge = findViewById(R.id.activity_main_til_age);
        tilBirthday = findViewById(R.id.activity_main_til_birthday);

        btnRegister = findViewById(R.id.activity_main_btn_register);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v("RegisterPressed", tilRut.getEditText().getText().toString());
            }
        });
        Log.v("Debugging", "Se creó el activity" );
    }
}